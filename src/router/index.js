import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  },
  {
    path: '/front/channel/:channel/email/:email/agent_email/:agent_email/premiun/:premium/responder_id/:responder_id/profile/:profile/group/:group/client_name/:client_name/conversation_id/:conversation_id',
    name: 'OcasaFront',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "about" */ '@/views/ocasa/FrontOffice.vue')
  },
  {
    path: '/back',
    name: 'OcasaBack',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import( /* webpackChunkName: "about" */ '@/views/ocasa/BackOffice.vue'),
    meta: {
        requiresAuth: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

export default router
