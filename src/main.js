import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import vSelect from 'vue-select'

// importing bootstrap
import {
  BootstrapVue,
  BootstrapVueIcons,
  IconsPlugin
} from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-select/dist/vue-select.css";

// global styles
import './styles/styles.sass'

// Install BootstrapVue
Vue.use(BootstrapVue,{
  // BFormFile: {
  //   "browseText": "Hola"
  // },
})

// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

// Optionally install the BootstrapVue icon components plugin
Vue.use(BootstrapVueIcons)

Vue.use(VueResource);

Vue.config.productionTip = false

Vue.use(vSelect)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
